/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import de.shd.api.cmdb.CMDBMonitoringConfiguration;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Steve Groeber <steve.groeber@shd-online.de>
 */
public class MonitoringConfigurationsModule extends MonitoringConfigurationFactory implements MethodConstants {

    private final SMViewClient client;

    public MonitoringConfigurationsModule(SMViewClient client) {
        this.client = client;
    }

    /**
     * Read the monitoring configurations.
     *
     * @see CMDBMonitoringConfiguration
     * @return Collection of all monitoring configurations
     * @throws SMViewClientException
     */
    public List<CMDBMonitoringConfiguration> read() throws SMViewClientException {
        JSONObject responseObject = client.sendRequest(SHD_MONITORING_CONFIGURATIONS_READ, new JSONObject(), "1");
        return createMonitoringConfigurations(responseObject);
    }

    private List<CMDBMonitoringConfiguration> createMonitoringConfigurations(JSONObject jsonObject) throws SMViewClientException {
        try {
            List<CMDBMonitoringConfiguration> cmdbMoniConfs = new ArrayList<>();
            JSONArray resultObjects = jsonObject.getJSONArray("result");

            for (int i = 0; i < resultObjects.length(); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(i);
                cmdbMoniConfs.add(createCMDBMonitoringConfiguration(resultObject));
            }
            return cmdbMoniConfs;
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
