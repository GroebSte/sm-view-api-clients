/*
 * The MIT License
 *
 * Copyright 2017 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api.cmdb;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class QueryObject {

    private String category;
    private String property;
    private String value;
    private String comparison;

    public QueryObject(String category, String property, String value) {
        this.category = category;
        this.property = property;
        this.value = value;
    }

    public QueryObject(String category, String property, String value, String comparison) {
        this.category = category;
        this.property = property;
        this.value = value;
        this.comparison = comparison;
    }

    public String getCategory() {
        if (category == null) {
            category = "";
        }
        return category;
    }

    public String getProperty() {
        if (property == null) {
            property = "";
        }
        return property;
    }

    public String getValue() {
        if (value == null) {
            value = "";
        }
        return value;
    }

    public String getComparison() {
        if (comparison == null) {
            comparison = "=";
        }
        return comparison;
    }
}
