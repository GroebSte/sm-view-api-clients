/*
 * The MIT License
 *
 * Copyright 2017 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api.cmdb;

/**
 *
 * @author Steve Groeber <steve.groeber@shd-online.de>
 */
public class MonitoringConfigurationOptions {

    private String site;
    private boolean multisite;
    private boolean lockHosts;
    private boolean lockFolders;
    private int master;
    
    public MonitoringConfigurationOptions() {
        this.site = "";
        this.multisite = false;
        this.lockHosts = false;
        this.lockFolders = false;
        this.master = -1;
    }

    public MonitoringConfigurationOptions(String site, boolean multisite, boolean lockHosts, boolean lockFolders, int master) {
        this.site = site;
        this.multisite = multisite;
        this.lockHosts = lockHosts;
        this.lockFolders = lockFolders;
        this.master = master;
    }
    
    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public boolean isMultisite() {
        return multisite;
    }

    public void setMultisite(boolean multisite) {
        this.multisite = multisite;
    }

    public boolean isLockHosts() {
        return lockHosts;
    }

    public void setLockHosts(boolean lock_hosts) {
        this.lockHosts = lock_hosts;
    }

    public boolean isLockFolders() {
        return lockFolders;
    }

    public void setLockFolders(boolean lock_folders) {
        this.lockFolders = lock_folders;
    }

    public int getMaster() {
        return master;
    }

    public void setMaster(int master) {
        this.master = master;
    }
}
