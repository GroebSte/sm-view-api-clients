/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api.cmdb;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class CMDBFilter {

    private String id;
    private String title;
    private String objectTypeID;
    private int displayedRows = 50;
    private int page = 0;
    private String userID;
    private Collection<ColumnProperty> columnProperties;
    private Collection<QueryObject> queryObjects;

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getTitle() {
        if (title == null) {
            title = "";
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getObjectTypeID() {
        return objectTypeID;
    }

    public void setObjectTypeID(String objectTypeID) {
        this.objectTypeID = objectTypeID;
    }

    public int getDisplayedRows() {
        return displayedRows;
    }

    public void setDisplayedRows(int displayedRows) {
        this.displayedRows = displayedRows;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getUserID() {
        if (userID == null) {
            userID = "";
        }
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Collection<ColumnProperty> getColumnProperties() {
        if (columnProperties == null) {
            columnProperties = new ArrayList<>();
        }
        return columnProperties;
    }

    public Collection<QueryObject> getQueryObjects() {
        if (queryObjects == null) {
            queryObjects = new ArrayList<>();
        }
        return queryObjects;
    }
}
