/*
 * The MIT License
 *
 * Copyright 2017 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import de.shd.api.cmdb.CMDBFilter;
import de.shd.api.cmdb.ColumnProperty;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
class FilterFactory {

    CMDBFilter createCMDBFilter(JSONObject jsonObject) throws SMViewClientException {
        try {
            CMDBFilter cmdbFilter = new CMDBFilter();
            cmdbFilter.setID(jsonObject.optString("shd_filter__id", ""));
            cmdbFilter.setTitle(jsonObject.optString("shd_filter__title", ""));
            cmdbFilter.setObjectTypeID(jsonObject.optString("shd_filter__obj_type_id", ""));
            cmdbFilter.setDisplayedRows(getInteger(jsonObject.optString("shd_filter__display_rows", "50")));
            cmdbFilter.setPage(getInteger(jsonObject.optString("shd_filter__page", "0")));
            cmdbFilter.setUserID(jsonObject.optString("shd_filter__user_id", ""));
            JSONArray columnObjects = jsonObject.optJSONArray("shd_filter__columns");
            if (columnObjects != null) {
                for (int j = 0; j < columnObjects.length(); j++) {
                    JSONObject columnObject = columnObjects.getJSONObject(j);
                    String id = columnObject.optString("id", "");
                    int width = getInteger(columnObject.optString("width", "80"));
                    String sort = columnObject.optString("sort", "");
                    cmdbFilter.getColumnProperties().add(new ColumnProperty(id, width, sort));
                }
            }
            JSONArray conditionObject = jsonObject.optJSONArray("shd_filter__condition");
            if (conditionObject != null) {
                for (int j = 0; j < conditionObject.length(); j++) {

                }
            }
            return cmdbFilter;
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }

    private int getInteger(String value) {
        return getInteger(value, 0);
    }

    private int getInteger(String value, int defaultValue) {
        int intValue = defaultValue;
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
        }
        return intValue;
    }
}
