/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class SMViewClientException extends Exception {

    private String message;
    private int code = 0;
    private String error;

    public SMViewClientException(JSONObject jsonObject) {
        super();
        init(jsonObject);
    }

    public SMViewClientException(String message) {
        this(message, "", 0);
    }

    public SMViewClientException(String message, String error) {
        this(message, error, 0);
    }

    public SMViewClientException(String message, String error, int code) {
        super();
        this.message = message;
        this.error = error;
        this.code = code;
    }

    public SMViewClientException(String message, Throwable cause) {
        super(cause);
        this.message = message;
    }

    @Override
    public String getMessage() {
        if (message == null) {
            message = "";
        }
        return message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        if (error == null) {
            error = "";
        }
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private void init(JSONObject jsonObject) {
        try {
            JSONObject errorObject = jsonObject.getJSONObject("error");
            this.message = errorObject.has("message") ? errorObject.getString("message") : "no message";
            this.code = errorObject.has("code") ? errorObject.getInt("code") : 0;

            if (errorObject.has("data")) {
                JSONObject dataObject = errorObject.getJSONObject("data");
                this.error = dataObject.has("error") ? dataObject.getString("error") : "";
            }
        } catch (JSONException ex) {
            this.message = ex.getLocalizedMessage();
        }
    }
}
