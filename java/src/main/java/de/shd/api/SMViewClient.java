/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @version 0.1
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class SMViewClient implements MethodConstants {

    private final String IDOIT_JSON_RCP_PATH = "/src/jsonrpc.php";
    private final Client client;

    private String url;
    private String apiKey;
    private String sessionID;
    private String clientID;

    /**
     * The IdoitClient handles all the communication with the idoit API.
     *
     * @param url i-doit URL
     * @param apiKey i-doit API-KEY
     */
    public SMViewClient(String url, String apiKey) {
        this.url = url;
        this.apiKey = apiKey;
        this.client = ClientBuilder.newClient();
    }

    /**
     * The URL for the i-doit system
     *
     * @return i-doit URL
     */
    public String getUrl() {
        if (url == null) {
            url = "";
        }
        return url;
    }

    /**
     * The current session ID
     *
     * @return i-doit session ID
     */
    public String getSessionID() {
        if (sessionID == null) {
            sessionID = "";
        }
        return sessionID;
    }

    /**
     * The current client ID
     *
     * @return i-doit client ID
     */
    public String getClientID() {
        if (clientID == null) {
            clientID = "";
        }
        return clientID;
    }

    /**
     * Create a i-doit session
     *
     * @param username the username
     * @param password the password
     * @throws SMViewClientException
     */
    public void login(String username, String password) throws SMViewClientException {
        try {
            WebTarget target = client.target(url + IDOIT_JSON_RCP_PATH);
            JSONObject request = createRequestObject(IDOIT_LOGIN, new JSONObject(), "1");
            MultivaluedMap<String, Object> authHeaders = new MultivaluedHashMap<>();
            authHeaders.add("X-RPC-Auth-Username", username);
            authHeaders.add("X-RPC-Auth-Password", password);
            Response response = target.request().headers(authHeaders).post(Entity.json(request.toString()));
            JSONObject responseObject = getResponseObject(response);
            JSONObject resultObject = responseObject.getJSONObject("result");
            sessionID = resultObject.getString("session-id");
            clientID = resultObject.getString("client-id");
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Close the i-doit session
     *
     * @throws SMViewClientException
     */
    public void logout() throws SMViewClientException {
        try {
            JSONObject responseObject = sendRequest(IDOIT_LOGOUT);
            JSONObject resultObject = responseObject.getJSONObject("result");
            if (resultObject.getBoolean("result")) {
                sessionID = "";
                clientID = "";
            }
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Access object query
     *
     * @return ObjectTypeModule
     */
    public FiltersModule filters() {
        return new FiltersModule(this);
    }

    /**
     * Access objects
     *
     * @return ObjectTypeModule
     */
    public FilterModule filter() {
        return new FilterModule(this);
    }
    
    /**
     * Access monitoring configuration query
     *
     * @return MonitoringConfigurationsModule
     */
    public MonitoringConfigurationsModule monitoringConfigurations() {
        return new MonitoringConfigurationsModule(this);
    }
    
    /**
     * Access monitoring configurations
     *
     * @return MonitoringConfigurationModule
     */
    public MonitoringConfigurationModule monitoringConfiguration() {
        return new MonitoringConfigurationModule(this);
    }

    JSONObject sendRequest(String method) throws SMViewClientException {
        return sendRequest(method, new JSONObject(), "1");
    }

    JSONObject sendRequest(String method, String id) throws SMViewClientException {
        return sendRequest(method, new JSONObject(), id);
    }

    JSONObject sendRequest(String method, JSONObject params, String id) throws SMViewClientException {
        WebTarget target = client.target(url + IDOIT_JSON_RCP_PATH);
        JSONObject request = createRequestObject(method, params, id);

        // for debuging only
        // System.out.println("SEND REQUEST: " + request.toString());
        Response response = getSessionID().isEmpty()
                ? target.request().post(Entity.json(request.toString()))
                : target.request().header("X-RPC-Auth-Session", getSessionID()).post(Entity.json(request.toString()));
        return getResponseObject(response);
    }

    private JSONObject createRequestObject(String method, JSONObject params, String id) throws SMViewClientException {
        try {
            JSONObject requestObject = new JSONObject();
            requestObject.put("method", method);
            params.put("apikey", apiKey);
            requestObject.put("params", params);
            requestObject.put("id", id);
            requestObject.put("jsonrpc", "2.0");
            return requestObject;
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private JSONObject getResponseObject(Response response) throws SMViewClientException {
        try {
            if (response.getStatus() == 200) {
                String responseString = response.readEntity(String.class);

                // for debuging only
                // System.out.println("GET RESPONSE: " + responseString);
                JSONObject responseObject = new JSONObject(responseString);
                if (responseObject.has("error")) {
                    throw new SMViewClientException(responseObject);
                }
                return responseObject;
            } else {
                throw new SMViewClientException(response.getStatusInfo().getReasonPhrase());
            }
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }
}
