/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import de.shd.api.cmdb.CMDBMonitoringConfiguration;
import de.shd.api.cmdb.MonitoringConfigurationOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Steve Groeber <steve.groeber@shd-online.de>
 */
public class MonitoringConfigurationModule extends MonitoringConfigurationFactory implements MethodConstants {

    private final SMViewClient client;

    public MonitoringConfigurationModule(SMViewClient client) {
        this.client = client;
    }

    /**
     * Create a filter.
     *
     * @param newTitle
     * @param type
     * @param path
     * @param address
     * @param options
     * 
     * @see CMDBFilter
     * @return the created filter
     * @throws SMViewClientException
     */
    public CMDBMonitoringConfiguration create(String newTitle, String type,
            String path, String address,
            MonitoringConfigurationOptions options) throws SMViewClientException {
        try {
            JSONObject configParams = new JSONObject();
            configParams.put("title", newTitle);
            configParams.put("type", type);
            configParams.put("path", path);
            configParams.put("address", address);
            JSONObject obj = new JSONObject(options);
            configParams.put("options", obj.toString());

            JSONObject params = new JSONObject();
            params.put("export_config", configParams);
            
            JSONObject responseObject = client.sendRequest(SHD_MONITORING_CONFIGURATION_CREATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return read(String.valueOf(resultObject.getInt("response")));
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Read a filter with the given ID.
     *
     * @param id the filter ID
     *
     * @see CMDBFilter
     * @return the filter with the given ID
     * @throws SMViewClientException
     */
    public CMDBMonitoringConfiguration read(String id) throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("export_config_id", id);
            JSONObject responseObject = client.sendRequest(SHD_MONITORING_CONFIGURATION_READ, params, "1");
            return createCMDBMonitoringObject(responseObject);
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Update a filter with the given ID.
     *
     * @param id the filter ID
     * @param newTitle new filter title
     * @param type
     * @param path
     * @param address
     * @param options
     *
     * @see CMDBFilter
     * @return update successful
     * @throws SMViewClientException
     */
    public boolean update(String id, String newTitle, String type,
            String path, String address,
            MonitoringConfigurationOptions options) throws SMViewClientException {
        try {
            JSONObject configParams = new JSONObject();
            configParams.put("title", newTitle);
            configParams.put("type", type);
            configParams.put("path", path);
            configParams.put("address", address);
            JSONObject obj = new JSONObject(options);
            configParams.put("options", obj.toString());

            JSONObject params = new JSONObject();
            params.put("export_config_id", id);
            params.put("export_config", configParams);

            JSONObject responseObject = client.sendRequest(SHD_MONITORING_CONFIGURATION_UPDATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            String message = resultObject.getString("message");
            return message.equals("monitoring.updateExportConfigs.success");
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Delete a filter with the given ID.
     *
     * @param id the filter ID
     *
     * @see CMDBFilter
     * @return delete successful
     * @throws SMViewClientException
     */
    public boolean delete(String id) throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("export_config_id", id);
            JSONObject responseObject = client.sendRequest(SHD_MONITORING_CONFIGURATION_DELETE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            String message = resultObject.getString("message");
            return message.equals("monitoring.deleteExportConfigs.success");
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private CMDBMonitoringConfiguration createCMDBMonitoringObject(JSONObject jsonObject) throws SMViewClientException {
        try {
            JSONArray resultObjects = jsonObject.getJSONArray("result");
            JSONObject resultObject = resultObjects.getJSONObject(0);
            return createCMDBMonitoringConfiguration(resultObject);
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
