/*
 * The MIT License
 *
 * Copyright 2017 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import de.shd.api.cmdb.CMDBMonitoringConfiguration;
import de.shd.api.cmdb.MonitoringConfigurationOptions;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Steve Groeber <steve.groeber@shd-online.de>
 */
class MonitoringConfigurationFactory {

    CMDBMonitoringConfiguration createCMDBMonitoringConfiguration(JSONObject jsonObject) throws SMViewClientException {
        try {
            CMDBMonitoringConfiguration cmdbMoniConf = new CMDBMonitoringConfiguration();
            cmdbMoniConf.setID(jsonObject.optString("isys_monitoring_export_config__id", ""));
            cmdbMoniConf.setTitle(jsonObject.optString("isys_monitoring_export_config__title", ""));
            cmdbMoniConf.setAddress(jsonObject.optString("isys_monitoring_export_config__address", ""));
            cmdbMoniConf.setPath(jsonObject.optString("isys_monitoring_export_config__path", ""));
            cmdbMoniConf.setType(jsonObject.optString("isys_monitoring_export_config__type", ""));
            if (jsonObject.isNull("isys_monitoring_export_config__options")) {
                return cmdbMoniConf;
            }
            String optionsObject = jsonObject.getString("isys_monitoring_export_config__options");
            JSONObject optionObject = new JSONObject(optionsObject);
            if (optionsObject.isEmpty()) {
                return cmdbMoniConf;
            }
            String site = optionObject.optString("site", "");
            int master = optionObject.optInt("master", -1);
            boolean multisite = optionObject.optBoolean("multisite", false);
            boolean lockHosts = optionObject.optBoolean("lock_hosts", false);
            boolean lockFolders = optionObject.optBoolean("lock_folders", false);
            cmdbMoniConf.setOptions(new MonitoringConfigurationOptions(site, multisite, lockHosts, lockFolders, master));
            return cmdbMoniConf;
        } catch(JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
