/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public interface MethodConstants {
    public final static String IDOIT_LOGIN = "idoit.login"; 
    public final static String IDOIT_LOGOUT = "idoit.logout"; 
    public final static String IDOIT_VERSION = "idoit.version"; 
    public final static String IDOIT_CONSTANTS = "idoit.constants"; 
    
    public final static String SHD_FILTERS_READ = "shd.filters.read";
    public final static String SHD_FILTER_READ = "shd.filter.read";
    public final static String SHD_FILTER_CREATE = "shd.filter.create";
    public final static String SHD_FILTER_UPDATE = "shd.filter.update";
    public final static String SHD_FILTER_DELETE = "shd.filter.delete";
    public final static String SHD_FILTER_QUERY = "shd.filter.query";
    
    public final static String SHD_MONITORING_CONFIGURATIONS_READ = "shd.monitoring.readExportConfigs";
    public final static String SHD_MONITORING_CONFIGURATION_READ = "shd.monitoring.readExportConfig";
    public final static String SHD_MONITORING_CONFIGURATION_CREATE = "shd.monitoring.createExportConfig";
    public final static String SHD_MONITORING_CONFIGURATION_UPDATE = "shd.monitoring.updateExportConfig";
    public final static String SHD_MONITORING_CONFIGURATION_DELETE = "shd.monitoring.deleteExportConfig";
}
