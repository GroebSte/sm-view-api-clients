/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import de.shd.api.cmdb.CMDBFilter;
import de.shd.api.cmdb.ColumnProperty;
import de.shd.api.cmdb.QueryObject;
import java.util.Collection;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class FilterModule extends FilterFactory implements MethodConstants {

    private final SMViewClient client;

    public FilterModule(SMViewClient client) {
        this.client = client;
    }

    /**
     * Create a filter.
     *
     * @param title the filter title
     * @param objectTypeID the object type ID or constant
     * @param displayRows
     * @param page
     * @param columnProperties
     * @param queryObjects
     *
     * @see CMDBFilter
     * @return the created filter
     * @throws SMViewClientException
     */
    public CMDBFilter create(String title, String objectTypeID, int displayRows, int page,
            Collection<ColumnProperty> columnProperties,
            Collection<QueryObject> queryObjects) throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("title", title);
            params.put("type", objectTypeID);
            JSONObject responseObject = client.sendRequest(SHD_FILTER_CREATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return read(String.valueOf(resultObject.getInt("id")));
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Read a filter with the given ID.
     *
     * @param id the filter ID
     *
     * @see CMDBFilter
     * @return the filter with the given ID
     * @throws SMViewClientException
     */
    public CMDBFilter read(String id) throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("filter_id", id);
            JSONObject responseObject = client.sendRequest(SHD_FILTER_READ, params, "1");
            return createCMDBObject(responseObject);
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Update a filter with the given ID.
     *
     * @param id the filter ID
     * @param newTitle new filter title
     * @param objectTypeID
     * @param displayRows
     * @param page
     * @param columnProperties
     * @param queryObjects
     *
     * @see CMDBFilter
     * @return update successful
     * @throws SMViewClientException
     */
    public boolean update(String id, String newTitle, String objectTypeID,
            int displayRows, int page,
            Collection<ColumnProperty> columnProperties,
            Collection<QueryObject> queryObjects) throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("filter_id", id);
            params.put("title", newTitle);
            JSONObject responseObject = client.sendRequest(SHD_FILTER_UPDATE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return resultObject.getBoolean("success");
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    /**
     * Delete a filter with the given ID.
     *
     * @param id the filter ID
     *
     * @see CMDBFilter
     * @return delete successful
     * @throws SMViewClientException
     */
    public boolean delete(String id) throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            params.put("filter_id", id);
            JSONObject responseObject = client.sendRequest(SHD_FILTER_DELETE, params, "1");
            JSONObject resultObject = responseObject.getJSONObject("result");
            return resultObject.getBoolean("success");
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private CMDBFilter createCMDBObject(JSONObject jsonObject) throws SMViewClientException {
        try {
            JSONObject resultObject = jsonObject.getJSONObject("result");
            return createCMDBFilter(resultObject);
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
