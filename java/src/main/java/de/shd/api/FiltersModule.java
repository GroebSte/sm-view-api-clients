/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api;

import de.shd.api.cmdb.CMDBFilter;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class FiltersModule extends FilterFactory implements MethodConstants {

    private final SMViewClient client;
    private String filterObjectType;

    public FiltersModule(SMViewClient client) {
        this.client = client;
    }

    /**
     * Read the filters.
     *
     * @see CMDBFilter
     * @return Collection of all filters
     * @throws SMViewClientException
     */
    public List<CMDBFilter> read() throws SMViewClientException {
        JSONObject responseObject = client.sendRequest(SHD_FILTERS_READ, createParams(), "1");
        return createCMDBFilters(responseObject);
    }

    /**
     * Filter filters with the given object type.
     *
     * @param objectType object type
     * @return FiltersModule
     */
    public FiltersModule filterObjectType(String objectType) {
        this.filterObjectType = objectType;
        return this;
    }

    private JSONObject createParams() throws SMViewClientException {
        try {
            JSONObject params = new JSONObject();
            if (filterObjectType != null) {
                params.put("type", filterObjectType);
            }
            return params;
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), ex.getCause());
        }
    }

    private List<CMDBFilter> createCMDBFilters(JSONObject jsonObject) throws SMViewClientException {
        try {
            List<CMDBFilter> cmdbFilters = new ArrayList<>();
            JSONArray resultObjects = jsonObject.getJSONArray("result");

            for (int i = 0; i < resultObjects.length(); i++) {
                JSONObject resultObject = resultObjects.getJSONObject(i);
                cmdbFilters.add(createCMDBFilter(resultObject));
            }
            return cmdbFilters;
        } catch (JSONException ex) {
            throw new SMViewClientException(ex.getLocalizedMessage(), jsonObject.toString());
        }
    }
}
