/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.shd.api.examples;

import de.shd.api.SMViewClient;
import de.shd.api.SMViewClientException;
import de.shd.api.cmdb.CMDBFilter;
import java.util.List;
import org.json.JSONException;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class Example02_Filters {

    public static void main(String[] args) throws JSONException {
        try {
            // URL und API-KEY in der Configuration Datei anpassen!
            // Client erstellen
            SMViewClient client = new SMViewClient(Configuration.IDOIT_URL, Configuration.API_KEY);
            
            // Login
            client.login("admin", "admin");
            
            // Alle Filter abfragen
            List<CMDBFilter> filters = client.filters().read();
            
            // Logout
            client.logout();

        } catch (SMViewClientException ex) {
            System.out.println("RPC-ERROR");
            System.out.println("---------");
            System.out.println("message: " + ex.getMessage());
            System.out.println("error: " + ex.getError());
            System.out.println("code: " + ex.getCode());
        }
    }
}
