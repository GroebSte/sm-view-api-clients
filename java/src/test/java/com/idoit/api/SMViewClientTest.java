/*
 * The MIT License
 *
 * Copyright 2016 Thomas Wittek <thomas.wittek@shd-online.de>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.idoit.api;

import de.shd.api.SMViewClient;
import de.shd.api.examples.Configuration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thomas Wittek <thomas.wittek@shd-online.de>
 */
public class SMViewClientTest {

    private SMViewClient client;

    public SMViewClientTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // IMPORTANT: set your test i-doit system
//        client = new IdoitClient("https://demo.i-doit.com", "c1ia5q");
        client = new SMViewClient(Configuration.IDOIT_URL, Configuration.API_KEY);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of login method, of class IdoitClient.
     */
    @Test
    public void testLogin() throws Exception {
        System.out.println("login");
        String username = "admin";
        String password = "admin";
        client.login(username, password);
        assertNotEquals("Session-ID", client.getSessionID(), "");
        assertNotEquals("Client-ID", client.getClientID(), "");
    }

    /**
     * Test of logout method, of class IdoitClient.
     */
    @Test
    public void testLogout() throws Exception {
        System.out.println("logout");
        String username = "admin";
        String password = "admin";
        client.login(username, password);
        client.logout();
        assertEquals("Session-ID", client.getSessionID(), "");
        assertEquals("Client-ID", client.getClientID(), "");
    }
}
